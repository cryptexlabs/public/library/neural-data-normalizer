class Normalizer {
    constructor(data) {
        if (data === void 0) {
            data = [];
        }
        this.dataset = [];
        this.datasetMeta = null;
        this.binaryInput = [];
        this.binaryOutput = [];
        this.outputProperties = [];
        this.dataset = data;
        if (this.dataset.length <= 0) {
            throw new Error("Normalizer input data shouldn't be empty");
        }
        if (Object.keys(this.dataset[0]).length <= 0) {
            throw new Error("Normalizer input data rows has to contain some properties (only 1st row is checked)");
        }
    }

    getOutputLength = function () {
        return this.outputProperties.length;
    };
    getOutputProperties = function () {
        return this.outputProperties;
    };
    getInputLength = function () {
        return this.binaryInput[0].length;
    };
    getBinaryInputDataset = function () {
        return this.binaryInput;
    };
    getBinaryOutputDataset = function () {
        return this.binaryOutput;
    };
    getDatasetMetaData = function () {
        return this.datasetMeta;
    };
    setDatasetMetaData = function (metadata) {
        this.datasetMeta = metadata;
        return this;
    };
    normalize = function (keepNull = true) {
        this.datasetMeta = (this.datasetMeta === null) ? this.analyzeMetaData(keepNull) : this.datasetMeta;
        const binaryInput = [];
        const binaryOutput = [];
        for (const i in this.dataset) {
            const row = this.dataset[i];
            let index = 0;
            let inputBits = [];
            let outputBits = [];
            for (const prop in row) {
                let bitsArr = void 0;
                const value = row[prop];
                const meta = this.datasetMeta[prop];
                switch (meta.type) {
                    case 'number':
                        bitsArr = [this.numToBit(meta.min, meta.max, value)];
                        break;
                    case 'boolean':
                        bitsArr = [this.boolToBit(value)];
                        break;
                    case 'string':
                        bitsArr = this.strToBitsArr(meta.distinctValues, value);
                        break;
                    case 'array':
                        bitsArr = this.arrToBitsArr(meta.distinctValues, value);
                        break;
                    default:
                        break;
                }
                if (this.outputProperties.indexOf(prop) > -1) {
                    outputBits = outputBits.concat(bitsArr);
                }

                inputBits = inputBits.concat(bitsArr);

                index++;
            }
            if (inputBits.length > 0) {
                this.binaryInput.push(inputBits);
            }
            if (outputBits.length > 0) {
                this.binaryOutput.push(outputBits);
            }
        }
    };
    analyzeMetaData = function (keepNull = true) {
        const firstRow = this.dataset[0];
        const distinctProps = this.distinctProps(firstRow);
        const distinctTypes = this.distinctTypes(firstRow);
        const metadata = {};
        const bitDataset = [];
        for (let _i = 0, distinctProps_1 = distinctProps; _i < distinctProps_1.length; _i++) {
            const prop = distinctProps_1[_i];
            const type = distinctTypes[prop];
            metadata[prop] = {
                type: type,
                min: null,
                max: null,
                distinctValues: null,
            };
            switch (type) {
                case 'number':
                    const minMax = this.getMinMax(prop, this.dataset);
                    metadata[prop].min = minMax[0];
                    metadata[prop].max = minMax[1];
                    break;
                case 'boolean':
                    metadata[prop].min = 0;
                    metadata[prop].max = 1;
                    break;
                case 'string':
                    const distinctStrVals = this.getDistinctVals(prop, this.dataset, keepNull);
                    metadata[prop].distinctValues = distinctStrVals;
                    break;
                case 'array':
                    const distinctArrVals = this.getDistinctArrayVals(prop, this.dataset, keepNull);
                    metadata[prop].distinctValues = distinctArrVals;
                    break;
            }
        }
        return metadata;
    };
    setOutputProperties = function (props) {
        this.outputProperties = props;
        return this;
    };
    getMinMax = function (prop, data) {
        let min = null;
        let max = null;
        for (const i in data) {
            const val = data[i][prop];
            if (min === null || val < min) {
                min = val;
            }
            if (max === null || val > max) {
                max = val;
            }
        }
        return [min, max];
    };
    getDistinctVals = function (property, data, keepNull = true) {
        const count = 0;
        const distinctValues = [];
        for (let _i = 0, data_1 = data; _i < data_1.length; _i++) {
            const row = data_1[_i];
            const val = row[property];
            if (distinctValues.indexOf(val) === -1 && (keepNull || val !== null)) {
                distinctValues.push(val);
            }
        }
        return distinctValues;
    };
    getDistinctArrayVals = function (property, data, keepNull = true) {
        const count = 0;
        const distinctValues = [];
        for (let _i = 0, data_2 = data; _i < data_2.length; _i++) {
            const row = data_2[_i];
            const arrVal = row[property];
            for (let _a = 0, arrVal_1 = arrVal; _a < arrVal_1.length; _a++) {
                const val = arrVal_1[_a];
                if (distinctValues.indexOf(val) === -1 && (keepNull || val !== null)) {
                    distinctValues.push(val);
                }
            }
        }
        return distinctValues;
    };
    numToBit = function (min, max, value) {
        return (value - min) / (max - min);
    };
    boolToBit = function (val) {
        return +val;
    };
    strToBitsArr = function (distinctValues, val) {
        const bitArr = new Array(distinctValues.length);
        bitArr.fill(0);
        for (const i in distinctValues) {
            if (val === distinctValues[i]) {
                bitArr[i] = 1;
            }
        }
        return bitArr;
    };
    arrToBitsArr = function (distinctValues, vals) {
        const bitArr = new Array(distinctValues.length);
        bitArr.fill(0);
        for (const j in vals) {
            const val = vals[j];
            const idx = distinctValues.indexOf(val);
            bitArr[idx] = 1;
        }
        return bitArr;
    };
    distinctProps = function (row) {
        return Object.keys(row);
    };
    distinctTypes = function (row) {
        const distinctTypes = {};
        for (const prop in row) {
            const value = row[prop];
            if (typeof value === 'object' && Array.isArray(value)) {
                distinctTypes[prop] = 'array';
            } else if (typeof value === 'object') {
                distinctTypes[prop] = 'object';
            } else {
                distinctTypes[prop] = typeof (value);
            }
        }
        return distinctTypes;
    };

}

module.exports = {Normalizer}