declare type Data = (number|string)[];

export interface NormalizedMetadata {
    distinctValues: Data;
    type: string;
    min: number;
    max: null;
}

export declare class Normalizer {
    constructor(data: any[]);
    public getBinaryOutputDataset(): Array<Array<number>>;
    public getBinaryInputDataset(): Array<Array<number>>
    public getDatasetMetaData(): Record<string, NormalizedMetadata>;
    public normalize(keepNull?: boolean): void;
    public setOutputProperties(props: string[]): Normalizer;
    public getInputLength(): number;
    public getOutputProperties(): string[];
    public analyzeMetaData(keepNull?: boolean): NormalizedMetadata
    public getMinMax(prop, data): [number, number];
    public getDistinctVals(property: string, data: Data, keepNull?: boolean): Data
    public getDistinctArrayVals(property: string, data: Data[], keepNull?: boolean): Data
    public distinctProps(row: Object): string[];
    public distinctTypes(row): Record<string, string>;
    public getOutputLength(): number;
    public setDatasetMetaData(metadata): Normalizer;
}